requests
pika==0.13.1
influxdb
flask
flask_request_params
pyyaml
paramiko
docker
