CONTROLLER_ID = 9

MYSQL_USER = "necos"
MYSQL_PASSWORD = 'wanPass!'
MYSQL_HOST = 'wan-master-mysql'
MYSQL_DB = "wan_slice_controller"

AGENTS_TIMEOUT = 5


AGENTS = {
    2 : {
       "agent_address" : "ip-address",
       "agent_port" : 3030,
       "description" : "unicamp wan agent",
       "tunnel_address" : "",
   },

    4 : {
    "agent_address" : "ip-address",
    "agent_port" : 23000,
    "description" : "ufscar wan agent",
    "tunnel_address" : "",
    },

   5 : {
       "agent_address" : "ip-address",
       "agent_port" : 3030,
       "description" : "5tonic wan agent",
       "tunnel_address" : ""
   },
  
    6 : {
    "agent_address" : "ip-address",
    "agent_port" : 3030,
    "description" : "uom wan agent",
    "tunnel_address" : "",
    }
}