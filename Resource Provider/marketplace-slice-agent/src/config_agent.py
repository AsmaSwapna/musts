AGENT_IP_PORT = "localhost:8001"
BROKER_IP_PORT = "localhost:8000"
MONGO_IP_PORT = "localhost:27017"
DB_NAME = "agentdb"
COLLECTION_DC = 'agent_dc_pdts'
COLLECTION_LOCAL_RESOURCES_DC = 'agent_local_resources_dc'
COLLECTION_NET = 'agent_net_pdts'
COLLECTION_LOCAL_RESOURCES_NET = 'agent_local_resources_net'

location = 'AMERICA.BRAZIL'
providerType = 'dc/net'
#DC_TYPE = "DC"
DC_TYPE = "EDGE"


dc_slice_provider = "DC UNICAMP"
dc_ip = "10.0.0.1"
dc_port = 3000
dc_cost=50
dc_id = 1
dc_EXPTIME = 30


wan_slice_provider = "WAN UNICAMP"
wan_ip = "10.0.0.1"
wan_port = 3000
wan_cost=50
wan_id = 1
wan_EXPTIME = 30

FIRST_LOCAL_RESOURCES_DC = {"_id": dc_id,
    # "model": "CORE2DUO",
    "architecture": "X86_64",
    "vendor": "INTEL",
    "number": 30000000,
    "storage_gb": 20000000,
    "memory_mb": 8192000000}

FIRST_LOCAL_RESOURCES_NET = [{"dc-part1": "core-dc",  "dc-part2": "edge-dc-slice-brazil",  "bandwidth-GB": 100000000000},
                        {"dc-part1": "core-dc",  "dc-part2": "edge-dc-slice-greece",  "bandwidth-GB": 100000000000}]
 